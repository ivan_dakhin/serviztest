<?php

/*
 * Plugin Name: Before sending CF7
 */

add_action( 'init', 'true_register_post_type_init' ); // Использовать функцию только внутри хука init

function true_register_post_type_init() {
    $labels = array(
        'name' => 'Лиды',
        'singular_name' => 'Лид', // админ панель Добавить->Функцию
        'add_new' => 'Добавить лид',
        'add_new_item' => 'Добавить новый лид', // заголовок тега <title>
        'edit_item' => 'Редактировать лид',
        'new_item' => 'Новый лид',
        'all_items' => 'Все лиды',
        'view_item' => 'Просмотр лида на сайте',
        'search_items' => 'Искать лид',
        'not_found' =>  'Лид не найдено.',
        'not_found_in_trash' => 'В корзине нет лида.',
        'menu_name' => 'Лиды' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true, // показывать интерфейс в админке
        'has_archive' => true,
        'menu_icon' => 'dashicons-media-text', // иконка в меню
        'menu_position' => 3, // порядок в меню
        'supports' => array( 'title', 'custom-fields')
    );
    register_post_type('leads', $args);
}

add_filter( 'post_updated_messages', 'true_post_type_messages' );

function true_post_type_messages( $messages ) {
    global $post, $post_ID;

    $messages['leads'] = array( // leads - название созданного нами типа записей
        0 => '', // Данный индекс не используется.
        1 => sprintf( 'Лид обновлен. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
        2 => 'Параметр обновлён.',
        3 => 'Параметр удалён.',
        4 => 'Лид обновлен',
        5 => isset($_GET['revision']) ? sprintf( 'Лид восстановлен из редакции: %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
        6 => sprintf( 'Функция опубликована на сайте. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
        7 => 'Функция сохранена.',
        8 => sprintf( 'Отправлено на проверку. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        9 => sprintf( 'Запланировано на публикацию: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотр</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
        10 => sprintf( 'Черновик обновлён. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    );

    return $messages;
}

add_filter( 'wpcf7_before_send_mail', 'wpcf7_before_send_mail_start_function' );

function wpcf7_before_send_mail_start_function($cf7){

    $post_data = array(
        'post_title'    => sprintf('Лид на имя %s',isset($_POST['your-name']) ? $_POST['your-name'] : 'empty'),
        'post_status'   => 'draft',
        'post_type'     => 'leads',
        'post_author'   => 1,
        'post_category' => array(1)
    );
    $post_id = wp_insert_post($post_data, true);

    if(isset($_POST['your-name'])){
        update_post_meta($post_id , 'name', $_POST['your-name']);
    }
    if(isset($_POST['your-email'])){
        update_post_meta($post_id , 'email', $_POST['your-email']);
    }
    if(isset($_POST['your-phone'])){
        update_post_meta($post_id , 'phone', $_POST['your-phone']);
    }
    if(isset($_POST['your-message'])){
        update_post_meta($post_id , 'message', $_POST['your-message']);
    }

}