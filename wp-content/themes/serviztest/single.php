<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package serviztest
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            <?php while ( have_posts() ) : the_post();?>
                <div class="post-content">
                    <div class="post-content-ct nw">
                        <div class="post-content-text">
                            <h2><?php echo the_title();?> </h2>
                            <?php echo the_content();?>
                            <button type="button" class="post-content-text-button">Купить</button>
                        </div>
                        <div class="post-content-image">
                            <?php $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); // возвращает массив параметров миниатюры ?>
                            <a data-fancybox="images" href="<?php echo $thumbnail_attributes[0]; ?>"><img src="<?php echo $thumbnail_attributes[0]; ?>"></a>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
