<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package serviztest
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main-ct nw">

            <div class="blog">
                <div class="blog-сt nw">
                    <div class="blog-items">
                        <?php while (have_posts()) : the_post();?>
                            <div class="blog-item">
                                <h2><?php the_title();?></h2>
                                <?php $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); // возвращает массив параметров миниатюры ?>
                                <div class="blog-item-image">
                                    <img src="<?php echo $thumbnail_attributes[0]; ?>">
                                </div>
                                <a href="<?php the_permalink(); ?>">Посмотреть товар</a>
                            </div>
                        <?php endwhile; ?>

                    </div>

                </div>
            </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
