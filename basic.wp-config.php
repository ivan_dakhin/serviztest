<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */ 

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'serviztest' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',.9?qqPwPMZxXmX}?dw<hlm:|p}cEzs(QOE44dW4<?^cg5!T/hWtjO2}gB[sD}X:');
define('SECURE_AUTH_KEY',  'doI2V]/(zM[QMQ3bg0$7`{sKL&Pa8%6#>O1Z_,Zg+0!Ho1gC<Q$))d~{9g/|JJD#');
define('LOGGED_IN_KEY',    'g(fqvF2+rA,,o5}>pD1a4sDBM1R.3s_hh8XT$Oq}mnB=|pOSET~?yh/?u-Dz$e&#');
define('NONCE_KEY',        'Xc.VjWMsR`5`. Y_.~KLlW5$;xq|YP`%,{;H=%#nu+h@`k;gu:YD_SHH+thd#T<_');
define('AUTH_SALT',        'Q{L-Z+f<:JB32?q^/lVm|^OFt,aH3.r!wYBm&ZO]G-8hnGx71*<X#Kp}66Ey8HbS');
define('SECURE_AUTH_SALT', '_gw d87QO8;G-uClw$uU0Nb2mu^;}>1++!^)tij?c>e]%d&O|nLXF$$-GA!Aa}U?');
define('LOGGED_IN_SALT',   '`k9_4 RESg]TD[@iGv;B/dL24)v&{-b11}N=*:jMM]5V6?aO|ZE>9;qu`+o&XyBh');
define('NONCE_SALT',       '( 2od.[C++k~G</$%8ex9ReU-V>nzn?+ +Js5ZR]J];XYfH6EQF{[G+Oc6C{!]=;');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
